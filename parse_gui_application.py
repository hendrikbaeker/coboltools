import tkinter
from typing import List

from cobolparser.cobolprogram import build_cobol_program, CobolProgram
from cobolparser.source import build_source_file


def scale_y(line_no: int, line_count: int) -> int:
    return int(50 + line_no * 900 / line_count)


def draw_next_column(canvas, program: CobolProgram, register_coverage: List[int], method_name: str, column_no: int):
    canvas.create_text(450 + column_no * 300, 20, text=method_name)
    b = program.get_method_coverage(method_name)
    line_count = program.line_count
    for i in b:
        y = scale_y(i, line_count)
        color = "grey" if i in register_coverage else "lightgrey"
        canvas.create_line(350 + column_no * 300, y, 550 + column_no * 300, y, fill=color)


def draw(program: CobolProgram, method_names: List[str]):
    register_coverage = program.get_register_coverage()

    line_count = program.line_count

    root = tkinter.Tk()
    canvas = tkinter.Canvas(root, width=300 * (len(method_names) + 1), height=1000)
    canvas.configure(background="white")
    canvas.pack()

    canvas.create_text(150, 20, text="D-Bereiche")

    for i in register_coverage:
        y = scale_y(i, line_count)
        canvas.create_line(50, y, 250, y, fill="grey")

    for i in range(0, int(line_count), 1000):
        y = scale_y(i, line_count)
        canvas.create_line(0, y, 300 * (len(method_names) + 1), y, fill="red")
        canvas.create_text(25, y - 10, text=str(i))

    for i in range(0, len(method_names)):
        draw_next_column(canvas, program, register_coverage, method_names[i], i)

    root.mainloop()


if __name__ == "__main__":
    source_path = "/home/hendrik/idl/vm_shared/cobolSources/IDL210/cobol/KC1002.CBL"
    source = build_source_file(source_path)
    program = build_cobol_program(source)
    program.print_call_dependencies("MAIN")

    draw(program, ["GET-COL-DESC-OVV", "CHECK-MAP-OVV", "GET-TAB-LINE-OVV"])
