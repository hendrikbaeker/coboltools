from typing import List, Dict

import re

from cobolparser.cobolmethod import CobolMethod, AbstractCobolMethod, ExternalCobolMethod
from cobolparser.register import Register
from cobolparser.source import SourceFile


class CobolProgram:
    method_dictionary: Dict[str, CobolMethod]
    registers: List[Register]

    def __init__(self, cobol_methods: List[CobolMethod], registers: List[Register], count:int) -> None:
        self.method_dictionary = {method.name: method for method in cobol_methods}
        self.registers = registers
        self.line_count = count

    def get_recursively_called_methods(self, method_name: str) -> List[AbstractCobolMethod]:

        # method_dictionary = {cobolMethod.name: cobolMethod for cobolMethod in allMethods}

        successors_with_duplicates = self._add_children_to_list([method_name], method_name)
        successors_without_duplicates = list(dict.fromkeys(successors_with_duplicates))

        methods_outside_this_source_file = [x for x in successors_without_duplicates if x not in self.method_dictionary.keys()]
        outside_methods_dictionary = {name: ExternalCobolMethod(name) for name in methods_outside_this_source_file}

        all_methods_dictionary = {**self.method_dictionary, **outside_methods_dictionary}
        self.method_dictionary = all_methods_dictionary

        return [all_methods_dictionary[successors] for successors in successors_without_duplicates]

    def _add_children_to_list(self, current_list: List[str], current_node: str) -> List[str]:
        try:
            child_methods = self.method_dictionary[current_node].get_all_called_methods()
        except:
            child_methods = []

        for child_method_name in child_methods:
            current_list.append(child_method_name)
            self._add_children_to_list(current_list, child_method_name)
        return current_list

    def print_call_dependencies(self, method_name: str):
        relevant_methods = self.get_recursively_called_methods(method_name)
        if len(relevant_methods) == 0:
            return

        for relevant_method in relevant_methods:
            if len(relevant_methods) == 0:
                continue
            for calls in relevant_method.get_all_called_methods():
                called_method = self.method_dictionary[calls]
                print(relevant_method.render() + "->" + called_method.render())

    def get_registers(self) -> List[Register]:
        return self.registers

    def get_register_coverage(self) -> List[int]:
        return [x for register in self.get_registers() for x in register.get_covered_lines()]

    def get_method_coverage(self, method_name: str) -> List[int]:
        return [x for method in self.get_recursively_called_methods(method_name) for x in method.get_covered_lines()]


def register_no_of(line: str) -> str:
    anfang = ".*EINGABE-(\\d\\d)-(ANFANG|ENDE).*"
    segments = re.match(anfang, line)
    return segments.group(1)


def build_registers(source_file: SourceFile) -> List[Register]:
    startingLines = source_file.get_line_numbers_containing_regex(".*EINGABE-\\d\\d-ANFANG.*")
    register_to_starting_line = {register_no_of(source_file.get_line(line_no)): line_no for line_no in startingLines}

    endingLines = source_file.get_line_numbers_containing_regex(".*EINGABE-\\d\\d-ENDE.*")
    register_to_ending_line = {register_no_of(source_file.get_line(line_no)): line_no for line_no in endingLines}

    return [Register(x, register_to_starting_line[x] + 1, register_to_ending_line[x] + 2) for x in register_to_starting_line.keys()]


def method_is_relevant(method: CobolMethod) -> bool:
    method_is_comment = bool(re.search("\\*.*", method.name))
    return not (method_is_comment or method.name in ["CONFIGURATION", "WORKING-STORAGE", "INPUT-OUTPUT", "LINKAGE", "FILE"])


def build_cobol_program(source_file: SourceFile) -> CobolProgram:
    section_line_numbers = source_file.get_line_numbers_containing_regex("[^\\*]*SECTION\\.")

    c_methods = [source_file.get_cobol_method_for_line_number(ln) for ln in section_line_numbers]
    filtered_methods = [method for method in c_methods if method_is_relevant(method)]

    registers = build_registers(source_file)
    return CobolProgram(filtered_methods, registers, source_file.get_line_count())
