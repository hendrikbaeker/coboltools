from typing import List


class Register:

    def __init__(self, register_no: int, start: int, end: int) -> None:
        self.register_no = register_no
        self.start = start
        self.end = end

    def get_covered_lines(self) -> List[int]:
        return list(range(self.start, self.end))
