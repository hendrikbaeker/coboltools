from typing import Dict, List

from cobolparser.cobolmethod import CobolMethod, build_cobol_method
import re


class SourceFile:

    def __init__(self, referenced_lines: Dict[int, str]) -> None:
        self.referenced_lines = referenced_lines

    def get_collection_of_lines(self, start: int, end: int) -> List[str]:
        return [self.referenced_lines[i] for i in range(start, end)]

    def get_line_count(self)->int:
        return len(self.referenced_lines)

    def get_line(self, line_no:int)->str:
        return self.referenced_lines[line_no]

    def get_line_numbers_containing_regex(self, regex: str) -> List[int]:
        return [i for i in self.referenced_lines.keys() if bool(re.search(regex, str(self.referenced_lines[i])))]

    def get_cobol_method_for_line_number(self, defining_line_number: int) -> CobolMethod:
        current_line_number = defining_line_number - 1
        while self._line_is_comment(current_line_number):
            current_line_number = current_line_number - 1

        start = current_line_number

        current_line_number = defining_line_number
        while not self._line_is_last_line_in_section(current_line_number):
            current_line_number = current_line_number + 1

        end = current_line_number

        return build_cobol_method(self.referenced_lines[defining_line_number],
                                  defining_line_number,
                                  start, end,
                                  self.get_collection_of_lines(start + 1, end + 1))

    def _line_is_comment(self, line_number: int) -> bool:
        line_content = self.referenced_lines[line_number].strip()
        return bool(re.search("\\*.*", line_content))

    def _line_is_last_line_in_section(self, line_number: int) -> bool:
        if line_number >= len(self.referenced_lines):
            return True
        line_content = self.referenced_lines[line_number].strip()
        return bool(re.search("EXIT\\..*", line_content))


def convert_to_utf8(by: bytes) -> str:
    try:
        return by.decode("utf-8")
    except:
        return str(by)


def build_source_file(source_path: str) -> SourceFile:
    opened_source_file = open(source_path, 'rb')
    content: list = opened_source_file.readlines()
    line_references = {index: convert_to_utf8(content[index]) for index in range(1, len(content))}
    return SourceFile(line_references)
