from abc import abstractmethod

from typing import List, Dict
import re


class AbstractCobolMethod:

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def get_all_called_methods(self):
        pass

    @abstractmethod
    def render(self) -> str:
        pass

    @abstractmethod
    def get_covered_lines(self) -> List[int]:
        pass


class CobolMethod(AbstractCobolMethod):

    def __init__(self, name: str, line_no: int,
                 start: int, end: int, source_code: List[str]) -> None:
        self.name = name
        self.line_no = line_no
        self.start = start
        self.end = end
        self.source_code = source_code

    def get_all_called_methods(self) -> List[str]:
        called_methods = list()
        for line in self.source_code:
            segments = re.match("\\s*PERFORM\\s([^\\s]*)", line)
            if bool(segments):
                match = segments.group(1)
                if match == "VARYING" or match == "TEST-OUT" or match == "UNTIL" or match == "SQLDYN":
                    continue
                called_methods.append(segments.group(1))
        return list(dict.fromkeys(called_methods))

    def get_name(self):
        return self.name

    def render(self):
        return "[" + self.name + "]"

    def get_covered_lines(self) -> List[int]:
        return list(range(self.start, self.end))


class ExternalCobolMethod(AbstractCobolMethod):

    def __init__(self, name: str) -> None:
        self.name = name
        self.color = "green"

    def name(self):
        return self.name

    def get_all_called_methods(self) -> list:
        return []

    def pretty_print(self):
        print(self.name)

    def print_call_dependencies(self, colorlookup: Dict[str, str]):
        pass

    def render(self) -> str:
        return "[" + self.name + "{bg:green}]"

    def get_covered_lines(self) -> List[int]:
        return []


def build_external_cobol_method(name: str) -> ExternalCobolMethod:
    return ExternalCobolMethod(name)


def build_cobol_method(raw_section_line: str, line_no: int,
                       start: int, end: int,
                       source_code: List[str]) -> CobolMethod:
    segments = re.match("(\\s*)([^\\s]*)\\sSECTION.*", raw_section_line)
    return CobolMethod(segments.group(2), line_no, start, end, source_code)
